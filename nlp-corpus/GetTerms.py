#!/usr/bin/python

#get terms from token dictionary

import sys

tokenFile = open('token.dict','r')

res = open('result.txt','a')
res.write(' ')
line = tokenFile.readline()
while line:
    pos = line.find('\t')
    if pos < 0:
        line = tokenFile.readline()
        continue;
    else:
        terms = line[0:pos]
#        print "terms -->%s" %(terms)
        terms += ' '
        res.write(terms)
        line = tokenFile.readline()
res.close()
tokenFile.close()
print "process done!"
